/**
 * Group name: TAB
 * Tanner Wernecke, Adam Slattum, Brandon Butterworth, Thomas Shattuck
 * CS474 - Group Project 3
 * This work complies with the JMU Honor Code.
 */
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import javax.swing.JOptionPane;


public class Main {

	/**
	 * Method that handles all the prompting and computation.
	 *
	 * @param args None
	 * @throws IOException if there is an input exception
	 */
	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {

		Scanner scanner = new Scanner(System.in);

		//Create the class
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		}
		catch (Exception e)
		{
			// ignore
		}

		//Connect to the database
		try
		{
			Connection conn = DriverManager.getConnection("jdbc:mysql://mysql.cs.jmu.edu/Manuscript2018", "butterbt", "cs474");

			Statement stmt = null;
			ResultSet rs = null;
			try
			{
				String userChoice = "";
				stmt = conn.createStatement();

				// get all table names and add them to an ArrayList for error checking
				ArrayList<String> tableNames = new ArrayList<String>();
				stmt.executeQuery("SHOW TABLES");
				rs = stmt.getResultSet();
				while (rs.next())
				{
					String table = rs.getString("Tables_in_Manuscript2018");
					tableNames.add(table);
				}

				//Greetings
				System.out.println("Hello! Welcome to the Madison Manuscript App.");

				while(!userChoice.equals("0"))
				{
					//Options for user to select from
					System.out.println("To choose an option, please select the number associated with your choice and press enter.\n");
					System.out.println("Display data entry information for a table (1)");
					System.out.println("Display each item in a column or columns from a table (2)");
					System.out.println("Count the number of items from a table (3)");
					System.out.println("List all of the table names (4)");
					System.out.println("Display each item contained in a table (5)");
					System.out.println("Read the full text from a manuscript in a new window (6)");
					System.out.println("Select a row based on criteria (7)");
					System.out.println("Select a row based on multiple criterion (8)");
					System.out.println("Exit the application (0)");
					userChoice = scanner.nextLine();
					String tableName = "";
					String columnName = "";

					switch(userChoice)
					{
					case "0": 
						// Exiting the application!
						break;
					case "1":
						System.out.println("Please enter a table name for which you would like data entry information for.");
						tableName = scanner.nextLine();

						// go through the tablenames to check that the tablename entered is a valid one
						while (!tableNames.contains(tableName))
						{
							System.out.println("Table does not exist, please enter a valid table name.");
							tableName = scanner.nextLine();
						}
						try
						{
							// Execute a describe SQL query
							stmt.executeQuery("DESCRIBE " + tableName);
							rs = stmt.getResultSet();

							// Format output
							System.out.printf("|%-20s|%-20s|%-10s|%-10s|%-15s|%-15s|\n", "Field", "Type", "Null", "Key", "Default", "Extra");
							System.out.println("|--------------------|--------------------|----------|----------|---------------|---------------|");

							// Loop while there is still records to be printed
							while(rs.next())
							{
								String field = rs.getString("Field");
								String type = rs.getString("Type");
								String nullKey = rs.getString("Null");
								String key = rs.getString("Key");
								String def = rs.getString("Default");
								String extra = rs.getString("extra");
								System.out.printf("|%-20s|%-20s|%-10s|%-10s|%-15s|%-15s|\n", field, type, nullKey, key, def, extra);
							}
						}
						catch(SQLException sqlEx)
						{
							// Exit the application due to SQL error
							System.exit(0);
						}

						break;
					case "2":
						System.out.println("Please enter a table name for which you would like to see items from specific columns.");
						tableName = scanner.nextLine();

						// go through the tablenames to check that the tablename entered is a valid one
						while (!tableNames.contains(tableName))
						{
							System.out.println("Table does not exist, please enter a valid table name.");
							tableName = scanner.nextLine();
						}

						System.out.println("Please enter a column name or names.");
						System.out.println("If you would like to enter multiple columns, seperate each one with a blank space.");

						// get the column name you want to search in the table
						columnName = scanner.nextLine();
						StringTokenizer st = new StringTokenizer(columnName, " ");
						StringTokenizer st1 = new StringTokenizer(columnName, " ");
						ArrayList<String> columns = new ArrayList<String>();
						ArrayList<String> userColumns = new ArrayList<String>();

						// describe the table to get the result set needed
						stmt.executeQuery("DESCRIBE " + tableName);
						rs = stmt.getResultSet();

						while (rs.next())
						{
							String table = rs.getString("Field");
							columns.add(table);
						}

						// loop over each of the columns and tokenize based on a space
						while (st.hasMoreTokens())
						{
							String currentColumn = st.nextToken();
							userColumns.add(currentColumn);
							if (!columns.contains(currentColumn))
							{
								System.out.println("One of the columns you entered does not exist please re enter your columns.");
								columnName = scanner.nextLine();
								st = new StringTokenizer(columnName, " ");
								st1 = new StringTokenizer(columnName, " ");
								userColumns.clear();
							}
						}

						// print the correct amount of columns for formatting
						System.out.print("|");
						for (int i = 0; i < userColumns.size(); i++)
						{
							System.out.printf("%-20s|", userColumns.get(i));
						}
						String columnsToExecute = "";

						// Execute select query based on requested column/columns
						if (st1.countTokens() == 1)
						{
							stmt.executeQuery("SELECT " + st1.nextToken() + " from " + tableName);
						}
						else
						{
							columnsToExecute = "";
							while (st1.hasMoreTokens())
							{
								String nextCol = st1.nextToken();
								if (st1.hasMoreTokens())
								{
									columnsToExecute += nextCol + ",";
								}
								else
								{
									columnsToExecute += nextCol;
								}
							}
							stmt.executeQuery("SELECT " + columnsToExecute + " from " + tableName);
						}

						rs = stmt.getResultSet();
						int counter = 0;
						int counterMax = userColumns.size();
						System.out.print("\n|");
						for (int i = 0; i < counterMax; i++)
						{
							System.out.print("--------------------|");

						}
						// Print the columns
						System.out.println();
						while(rs.next())
						{
							System.out.print("|");
							System.out.printf("%-20s|", rs.getString(userColumns.get(counter)));
							counter++;
							while (counter < counterMax)
							{
								System.out.printf("%-20s|", rs.getString(userColumns.get(counter)));
								counter++;
							}
							counter = 0;
							System.out.println();
						}

						break;
					case "3":
						System.out.println("Please enter a table name for which you would like to display the number of items for.");
						tableName = scanner.nextLine();

						// check that the tablename given exists
						while (!tableNames.contains(tableName))
						{
							System.out.println("Table does not exist, please enter a valid table name.");
							tableName = scanner.nextLine();
						}
						try
						{
							// get the amount of columns from a specific table
							stmt.executeQuery("SELECT COUNT(*) FROM " + tableName);
							rs = stmt.getResultSet();
							if(rs.next())
							{
								System.out.println("There are " + rs.getInt(1) + " items in the " + tableName + " table.");
							}
						}
						catch(SQLException sqlEx)
						{
							System.out.println(sqlEx.getMessage());
						}

						break;
					case "4":
						//Try to show the tables in the database	
						try 
						{
							stmt.executeQuery("SHOW TABLES");
							rs = stmt.getResultSet();
							System.out.printf("|%-30s|\n", "   Tables in Manuscript2018");
							System.out.println("|------------------------------|");
							while (rs.next())
							{
								String table = rs.getString("Tables_in_Manuscript2018");
								System.out.printf("|%-30s|\n", table);
							}
						}
						catch(SQLException sqlEx)
						{
							System.out.println(sqlEx.getMessage());
						}
						break;
					case "5":
						System.out.println("Please enter a table name for which you would like to display all of the items for.");
						tableName = scanner.nextLine();

						//Make sure the table requested exists
						while (!tableNames.contains(tableName))
						{
							System.out.println("Table does not exist, please enter a valid table name.");
							tableName = scanner.nextLine();
						}

						try
						{
							//Select all from the table
							stmt.executeQuery("SELECT * FROM " + tableName);
							rs = stmt.getResultSet();
							java.sql.ResultSetMetaData metadata = rs.getMetaData();
							int columnCount = metadata.getColumnCount();
							String[] columnNames = new String[columnCount];

							//Get each column heading
							for (int i = 1; i <= columnCount; i++)
							{
								columnNames[i - 1] = metadata.getColumnName(i);
							}
							System.out.print("|");

							//Print each column Heading
							for (int i = 0; i < columnNames.length; i++)
							{
								System.out.printf("%-20s|", columnNames[i]);

							}

							//Print lines for formatting
							System.out.print("\n|");
							for (int i = 0; i < columnNames.length; i++)
							{
								System.out.print("--------------------|");

							}
							System.out.println();

							//Print all the values from the result set
							while(rs.next())
							{
								System.out.print("|");
								for (int i = 1; i <= columnCount; i++)
								{
									System.out.printf("%-20s|", rs.getString(i));
								}
								System.out.println();
							}
						}
						catch(SQLException sqlEx)
						{

						}
						break;
					case "6":
						//Get all the necessary information from the user to find a specific manuscript text
						System.out.print("Please enter a libSiglum: ");
						String libSiglum = scanner.nextLine();
						System.out.print("Please enter a msSiglum: ");
						String msSiglum = scanner.nextLine();
						System.out.print("Please enter a sectionID: ");
						String sectionID = scanner.nextLine();
						System.out.print("Please enter a leafNumber: ");
						String leafNumber = scanner.nextLine();
						System.out.print("Please enter a columnNumber: ");
						String columnNumber = scanner.nextLine();
						System.out.print("Please enter a lineNumber: ");
						String lineNumber = scanner.nextLine();
						stmt.executeQuery("SELECT msFullText FROM Chant WHERE libSiglum = \"" + libSiglum + "\" AND msSiglum = \"" + msSiglum + "\" AND sectionID = " + sectionID + " AND leafNumber = \"" + leafNumber + "\" AND columnNumber = " + columnNumber + " AND lineNumber = " + lineNumber);
						rs = stmt.getResultSet();
						rs.next();

						//Print the full text in a new window
						JOptionPane.showConfirmDialog(null, "<html><body><p style='width: 400px;'>"+rs.getString("msFullText")+"</p></body></html>", "Manuscript: " + libSiglum + ", " + msSiglum + ", " + sectionID + ", " + leafNumber + ", " + columnNumber + ", " + lineNumber, JOptionPane.PLAIN_MESSAGE);
						break;
					case "7":
						System.out.print("Which table do you want to select from? ");
						String table = scanner.nextLine();

						//Make sure the table exists
						while (!tableNames.contains(table))
						{
							System.out.println("Table does not exist, please enter a valid table name.");
							table = scanner.nextLine();
						}

						System.out.print("Which attribute do you want to select on? (");

						//Use this next section to display all possible attributes the user can select on
						stmt.executeQuery("SELECT * FROM " + table);
						rs = stmt.getResultSet();
						java.sql.ResultSetMetaData metadata = rs.getMetaData();
						int columnCount = metadata.getColumnCount();
						String[] columnNames = new String[columnCount];

						//Get all the column names
						for (int i = 1; i <= columnCount; i++)
						{
							columnNames[i - 1] = metadata.getColumnName(i);
						}

						//Print each column name
						for (int i = 0; i < columnNames.length; i++)
						{
							System.out.print(columnNames[i]);
							if (i != columnNames.length - 1) {
								System.out.print(", ");
							}
						}
						System.out.print(") ");

						//Execute the sql query and get information about the result set
						String attribute = scanner.nextLine();
						System.out.print("What value do you want this attribute to have? ");
						String value = scanner.nextLine();
						System.out.println();
						stmt.executeQuery("SELECT * FROM " + table + " WHERE " + attribute + " = \"" + value + "\"");
						rs = stmt.getResultSet();
						metadata = rs.getMetaData();
						columnCount = metadata.getColumnCount();
						columnNames = new String[columnCount];

						//Get the column names
						for (int i = 1; i <= columnCount; i++)
						{
							columnNames[i - 1] = metadata.getColumnName(i);
						}

						//Print the column names
						System.out.print("|");
						for (int i = 0; i < columnNames.length; i++)
						{
							System.out.printf("%-20s|", columnNames[i]);
						}
						System.out.print("\n|");
						for (int i = 0; i < columnNames.length; i++)
						{
							System.out.print("--------------------|");
						}
						System.out.println();

						//Print all the values from the result set
						while(rs.next())
						{
							System.out.print("|");
							for (int i = 1; i <= columnCount; i++)
							{
								System.out.printf("%-20s|", rs.getString(i));
							}
							System.out.println();
						}
						break;
					case "8":
						System.out.print("Which table do you want to select from? ");
						table = scanner.nextLine();

						//Check to make sure the table exists
						while (!tableNames.contains(table))
						{
							System.out.println("Table does not exist, please enter a valid table name.");
							table = scanner.nextLine();
						}

						//Get all the attributes the user wants to select on
						System.out.print("Which attributes do you want to select on? If you would like to enter multiple columns, seperate each one with a blank space. (");						
						stmt.executeQuery("SELECT * FROM " + table);
						rs = stmt.getResultSet();
						metadata = rs.getMetaData();
						columnCount = metadata.getColumnCount();
						columnNames = new String[columnCount];

						//Get all the column names
						for (int i = 1; i <= columnCount; i++)
						{
							columnNames[i - 1] = metadata.getColumnName(i);
						}

						//Print all the column names
						for (int i = 0; i < columnNames.length; i++)
						{
							System.out.print(columnNames[i]);
							if (i != columnNames.length - 1) {
								System.out.print(", ");
							}
						}
						System.out.print(") ");
						attribute = scanner.nextLine();

						//Figure out which attributes the user wants to use
						StringTokenizer tokenizer = new StringTokenizer(attribute, " ");
						ArrayList<String> attributes = new ArrayList<String>();
						while (tokenizer.hasMoreTokens())
						{
							String currentColumn = tokenizer.nextToken();
							attributes.add(currentColumn);
						}

						//Get all the values the user wants to use
						value = null;
						ArrayList<String> values = new ArrayList<String>();
						for (String s : attributes) {
							System.out.print("What value do you want " + s + " to have? ");
							value = scanner.nextLine();
							values.add(value);
						}

						//Execute the sql query
						String query = "SELECT * FROM " + table + " WHERE ";
						for (int i = 0; i < attributes.size(); i++) {
							query += attributes.get(i) + " = \"" + values.get(i) + "\"";
							if (i != attributes.size() - 1) {
								query += " AND ";
							}
						}
						stmt.executeQuery(query);
						rs = stmt.getResultSet();
						metadata = rs.getMetaData();
						columnCount = metadata.getColumnCount();
						columnNames = new String[columnCount];

						//Get all the resulting columns
						for (int i = 1; i <= columnCount; i++)
						{
							columnNames[i - 1] = metadata.getColumnName(i);
						}

						//Print the column names
						System.out.print("|");
						for (int i = 0; i < columnNames.length; i++)
						{
							System.out.printf("%-20s|", columnNames[i]);
						}
						System.out.print("\n|");
						for (int i = 0; i < columnNames.length; i++)
						{
							System.out.print("--------------------|");
						}
						System.out.println();

						//Print all values from the resulting set
						while(rs.next())
						{
							System.out.print("|");
							for (int i = 1; i <= columnCount; i++)
							{
								System.out.printf("%-20s|", rs.getString(i));
							}
							System.out.println();
						}
						break;
					default: 
						//If no other option is selected, then it's an invalid option
						System.out.println("That was an invalid option, please try again.");
						break;
					}
					System.out.println("\n");
				}

			}
			finally
			{
				// it is a good idea to release
				// resources in a finally{} block
				// in reverse-order of their creation
				// if they are no-longer needed
				if (rs != null)
				{
					try
					{
						rs.close();
					}
					catch (SQLException sqlEx)
					{
						// ignore
					}
					rs = null;
				}
				if (stmt != null)
				{
					try
					{
						stmt.close();
					}
					catch (SQLException sqlEx)
					{
						// ignore
					}
					stmt = null;
				}
			}
		} catch (SQLException e) {
			// handle any errors
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
		}


	}
	
	/**
	 * The method that can be used for printing to a file. It is not finished, but SQL gave us many errors that prohibited 
	 * us from correctly implementing it. We were not given write permission which is part of the reason why we were unable to implement it.
	 * 
	 * @param column to print
	 * @param scanner to read from
	 * @param rs result set from the query
	 * @throws FileNotFoundException if there is a file exception 
	 * @throws SQLException if there is a sql exception
	 */
	private static void queryToFile(String column, Scanner scanner, ResultSet rs) throws FileNotFoundException, SQLException
	{
		
		PrintWriter out = null;
		System.out.println("Would you like to print this query to a file?  Y/N");
		
		String choice = scanner.next().toUpperCase();
		String filename = "";
		
		if (choice.equals("Y"))
		{
			
			rs.beforeFirst();
			
			System.out.println("What would you like to call this file?");
			
			filename = scanner.next();
			
			out = new PrintWriter("/" + filename);
			
			out.println("Values in " + column);
			out.println("|-----------------------|");
			
			while (rs.next())
			{
				out.println("|" + rs.getString(column));
				
			}		
			
		} else {
			return;
		}
		
		
		out.close();
		
	}

}